const express = require('express');
const router = express();
const authRouter = require('./authRouter');
const userRouter = require('./userRouter');
const notesRouter = require('./notesRouter');

router.use('/auth', authRouter);
router.use('/users', userRouter);
router.use('/notes', notesRouter);

module.exports = router;
