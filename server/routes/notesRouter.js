const express = require('express');
const router = express();
const noteController = require('../controllers/noteController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/', authMiddleware, noteController.getAll);
router.post('/', authMiddleware, noteController.create);
router.get('/:_id', authMiddleware, noteController.getOne);
router.put('/:_id', authMiddleware, noteController.update);
router.patch('/:_id', authMiddleware, noteController.toggleCheck);
router.delete('/:_id', authMiddleware, noteController.delete);

module.exports = router;
