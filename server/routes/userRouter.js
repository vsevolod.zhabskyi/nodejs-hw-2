const express = require('express');
const router = express();
const userController = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, userController.get);
router.patch('/me', authMiddleware, userController.updatePassword);
router.delete('/me', authMiddleware, userController.delete);

module.exports = router;
