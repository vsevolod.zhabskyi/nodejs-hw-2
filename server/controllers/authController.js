const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

class AuthController {
  async login(req, res) {
    try {
      const {username, password} = req.body;

      const user = await User.findOne({username});
      if (!user) {
        return res.status(400).json({message: 'No user with such login'});
      }

      const comparePassword = bcrypt.compareSync(password, user.password);
      if (!comparePassword) {
        return res.status(400).json({message: 'Wrong password'});
      }

      const {_id} = user;
      const token = jwt.sign(
          {_id},
          process.env.SECRET_KEY,
          {expiresIn: '24h'},
      );

      return res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }

  async registration(req, res) {
    try {
      const {username, password} = req.body;

      if (!username || !password) {
        return res.status(400).json({message: 'No username or password'});
      }

      const existingUser = await User.findOne({username});
      if (existingUser) {
        return res.status(400).json({message: 'This username is already used'});
      }

      const hashPassword = await bcrypt.hash(password, 6);

      await User.create({
        username,
        password: hashPassword,
        createdDate: new Date(),
      });

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }
}

module.exports = new AuthController();
