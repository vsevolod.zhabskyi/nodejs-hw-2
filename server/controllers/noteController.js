const User = require('../models/User');

class NoteController {
  async getAll(req, res) {
    try {
      const {_id} = req.user;

      const user = await User.findOne({_id});
      const notes = user.notes;

      const offset = +req.query.offset || 0;
      const limit = +req.query.limit || 0;
      const count = notes.length;

      let notesPaginated;
      if (limit) {
        notesPaginated = notes.slice(offset, offset + limit);
      } else {
        notesPaginated = notes.slice(offset);
      }

      return res.status(200).json({
        offset,
        limit,
        count,
        notes: notesPaginated,
      });
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }

  async getOne(req, res) {
    try {
      const userId = req.user._id;

      const {_id} = req.params;

      const user = await User.findOne({_id: userId});

      const note = user.notes.id(_id);
      if (!note) {
        return res.status(400).json({message: 'No such note'});
      }

      return res.status(200).json({note});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }

  async create(req, res) {
    try {
      const {_id} = req.user;

      const {text} = req.body;
      if (!text || text === '') {
        return res.status(400).json({message: 'Text field cannot be empty'});
      }

      const user = await User.findOne({_id});

      user.notes.push({
        text,
        userId: _id,
        completed: false,
        createdDate: new Date(),
      });

      await user.save();

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }

  async update(req, res) {
    try {
      const userId = req.user._id;

      const {_id} = req.params;

      const {text} = req.body;
      if (!text || text === '') {
        return res.status(400).json({message: 'Text field cannot be empty'});
      }

      const user = await User.findOne({_id: userId});

      const note = user.notes.id(_id);
      if (!note) {
        return res.status(400).json({message: 'No such note'});
      }

      note.text = text;

      await user.save();

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }
  async toggleCheck(req, res) {
    try {
      const userId = req.user._id;

      const {_id} = req.params;

      const user = await User.findOne({_id: userId});

      const note = user.notes.id(_id);
      if (!note) {
        return res.status(400).json({message: 'No such note'});
      }

      note.completed = !note.completed;

      await user.save();

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }

  async delete(req, res) {
    try {
      const userId = req.user._id;

      const {_id} = req.params;

      const user = await User.findOne({_id: userId});

      const note = user.notes.id(_id);
      if (!note) {
        return res.status(400).json({message: 'No such note'});
      }

      note.remove();

      await user.save();

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }
}

module.exports = new NoteController();
