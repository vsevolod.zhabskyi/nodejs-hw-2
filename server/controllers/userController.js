const User = require('../models/User');
const bcrypt = require('bcrypt');

class UserController {
  async get(req, res) {
    try {
      const {_id} = req.user;

      const user = await User.findOne({_id});

      const {username, createdDate} = user;

      res.status(200).json({
        user: {
          _id: _id,
          username: username,
          createdDate: createdDate,
        }});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }

  async delete(req, res) {
    try {
      const {_id} = req.user;

      await User.deleteOne({_id});

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }

  async updatePassword(req, res) {
    try {
      const {_id} = req.user;
      const {oldPassword, newPassword} = req.body;

      if (!oldPassword || !newPassword) {
        return res.status(400).json({message: 'Client error'});
      }

      const user = await User.findOne({_id});

      const comparePassword = bcrypt.compareSync(oldPassword, user.password);
      if (!comparePassword) {
        return res.status(400).json({message: 'Wrong password'});
      }

      const hashPassword = await bcrypt.hash(newPassword, 6);
      user.password = hashPassword;

      await user.save();

      return res.status(200).json({message: 'Success'});
    } catch (e) {
      return res.status(500).json({message: 'Server error'});
    }
  }
}

module.exports = new UserController();
