const {Schema, model} = require('mongoose');
const NoteSchema = require('./NoteSchema');

const User = new Schema({
  username: {type: String, unique: true, required: true},
  password: {type: String, required: true},
  createdDate: {type: Date, required: true},
  notes: [NoteSchema],
});

module.exports = model('User', User);
