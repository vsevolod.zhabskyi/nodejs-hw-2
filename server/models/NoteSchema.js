const {Schema} = require('mongoose');

const NoteSchema = new Schema({
  text: {type: String, required: true},
  userId: {type: String, required: true},
  completed: {type: Boolean, required: true},
  createdDate: {type: Date, required: true},
});

module.exports = NoteSchema;
