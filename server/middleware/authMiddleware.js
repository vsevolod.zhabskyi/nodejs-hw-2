const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      return res.status(400).json({message: 'User is not logged in'});
    }

    const {_id} = jwt.verify(token, process.env.SECRET_KEY);
    req.user = {_id};

    next();
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};
