require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const router = require('./routes');
const logMiddleware = require('./middleware/logMiddleware');
const cors = require('cors');

const PORT = process.env.PORT || 8080;

const app = express();
app.use(cors());
app.use(express.json());
app.use('/', logMiddleware);
app.use('/api', router);

const start = async () => {
  try {
    await mongoose.connect(process.env.DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    app.listen(PORT, () => console.log('Server started on port ' + PORT));
  } catch (e) {
    console.log(e);
  }
};

start();

