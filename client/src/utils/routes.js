export const LOGIN_ROUTE = '/login'
export const REGISTRATION_ROUTE = '/registration'
export const NOTES_ROUTE = '/notes'
export const PROFILE_ROUTE = '/profile'
