import { observer } from 'mobx-react-lite';
import React, { useContext } from 'react';
import { Nav, Navbar, Container, Button } from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import { Context } from '../index';
import {NOTES_ROUTE} from '../utils/routes';
import {PROFILE_ROUTE} from '../utils/routes';

const NavBar = observer(() => {
  const {user} = useContext(Context)

  const logOut = () => {
    user.setUser(null)
    user.setIsAuth(false)
  }

  return (
    <div>
      {user.isAuth &&
        <Navbar bg="light" variant="light" style={{height: 70}}>
          <Container>
            <NavLink style={{color: 'black', fontSize: '26px', textDecoration: 'none'}} to={NOTES_ROUTE}>Notes</NavLink>
            <NavLink style={{color: 'black', fontSize: '26px', textDecoration: 'none'}} to={PROFILE_ROUTE}>Profile</NavLink>
            <Nav className="ml-auto">
              <Button
                variant='outline-dark'
                onClick={() => logOut()}
                className="mx-3"
              >
                Log out
              </Button>
            </Nav>
          </Container>
        </Navbar>
      }
    </div>
  );
})

export default NavBar;
