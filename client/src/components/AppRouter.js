import React, {useContext} from 'react';
import {Route, Routes, Navigate} from 'react-router-dom';
import {
  LOGIN_ROUTE,
  NOTES_ROUTE,
  PROFILE_ROUTE,
  REGISTRATION_ROUTE,
} from '../utils/routes';
import LoginRegister from '../pages/LoginRegister';
import Notes from '../pages/Notes';
import Profile from '../pages/Profile';
import {Context} from '../index';
import {observer} from 'mobx-react-lite';

const AppRouter = observer(() => {

  const {user} = useContext(Context)

  return (
    <Routes>
      <Route path={LOGIN_ROUTE} element={<LoginRegister/>}/>
      <Route path={REGISTRATION_ROUTE} element={<LoginRegister/>}/>
      {user.isAuth && (<>
        <Route path={PROFILE_ROUTE} element={<Profile/>}/>
        <Route path={NOTES_ROUTE} element={<Notes/>}/>
      </>)}
      <Route path="*" element={<Navigate to={LOGIN_ROUTE} replace/>}/>
    </Routes>
  );
})

export default AppRouter;
