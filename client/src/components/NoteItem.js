import React, {useState} from 'react';
import {Button, ButtonGroup, Form, ListGroupItem} from 'react-bootstrap';

function NoteItem(props) {

  const {note, checkNote, deleteNote, editNote} = props
  const [showEdit, setShowEdit] = useState(false);
  const [newText, setNewText] = useState(note.text);

  const handleEditNote = () => {
    editNote(note._id, newText)
      .then(() => {
        setNewText(newText);
        setShowEdit(false);
      })
  }

  return (
    <ListGroupItem key={note._id} className="p-2 d-flex align-items-center justify-content-between">
      {showEdit
        ?
        <Form.Control
          className="m-0"
          placeholder="New text..."
          value={newText}
          onChange={e => setNewText(e.target.value)}
        />
        :
        <p className="m-1">{note.text}</p>
      }
      <ButtonGroup>
        {showEdit && (
          <Button onClick={() => handleEditNote()}>Confirm</Button>
        )}
        <Button onClick={() => setShowEdit(!showEdit)}>
          {showEdit ? 'Cancel' : 'Edit'}
        </Button>
        <Button
          className={note.completed ? 'btn-dark' : 'btn-success'}
          onClick={() => checkNote(note._id)}
        >
          {note.completed ? 'Uncheck' : 'Check'}
        </Button>
        <Button className="btn-danger" onClick={() => deleteNote(note._id)}>Delete</Button>
      </ButtonGroup>
    </ListGroupItem>
  );
}

export default NoteItem;
