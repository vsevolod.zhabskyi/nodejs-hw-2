import jwt_decode from "jwt-decode";
import {$host} from './index';

export const registration = async (username, password) => {
  await $host.post('api/auth/register', {username, password})

  const {data} = await $host.post('api/auth/login', {username, password})

  localStorage.setItem('token', data.jwt_token)

  return jwt_decode(data.jwt_token)
}

export const login = async (username, password) => {
  const {data} = await $host.post('api/auth/login', {username, password})

  localStorage.setItem('token', data.jwt_token)

  return jwt_decode(data.jwt_token)
}
