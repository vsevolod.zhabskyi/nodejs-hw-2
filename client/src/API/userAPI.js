import {$authHost} from './index';

export const fetchUser = async () => {
  const {data} = await $authHost.get('api/users/me/')
  console.log(data.user);
  return data.user
}

export const deleteUser = async () => {
  await $authHost.delete('api/users/me/')
}

export const updatePassword = async (oldPassword, newPassword) => {
  await $authHost.patch('api/users/me/', {oldPassword, newPassword})
}


