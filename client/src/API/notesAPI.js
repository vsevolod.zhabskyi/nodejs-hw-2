import {$authHost} from './index';

export default class NotesAPI {
  static async fetchNotes() {
    const {data} = await $authHost.get('api/notes/')
    return data
  }

  static async createNote (text) {
    await $authHost.post('api/notes', {text})
  }

  static async checkNote (id) {
    await $authHost.patch(`api/notes/${id}`)
  }

  static async deleteNote (id) {
    await $authHost.delete(`api/notes/${id}`)
  }

  static async editNote (id, text) {
    await $authHost.put(`api/notes/${id}`, {text})
  }
}
