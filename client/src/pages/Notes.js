import React, {useEffect, useState} from 'react';
import {Button, Container, Form, ListGroup} from "react-bootstrap";
import NotesAPI from '../API/notesAPI';
import NoteItem from '../components/NoteItem';

function Notes() {

  const [notes, setNotes] = useState([]);
  const [showCreateNote, setShowCreateNote] = useState(false)
  const [newNoteText, setNewNoteText] = useState('')
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    NotesAPI.fetchNotes().then(data => {
      setNotes(data.notes);
      setIsLoading(false)
    })
  }, [])

  const handleAddNote = () => {
    NotesAPI.createNote(newNoteText)
      .then(() => NotesAPI.fetchNotes())
      .then(data => {
        setNotes(data.notes);
        setNewNoteText('');
      })
  }

  const handleCheckNote = (id) => {
    NotesAPI.checkNote(id)
      .then(() => NotesAPI.fetchNotes())
      .then(data => setNotes(data.notes))
  }

  const handleDeleteNote = (id) => {
    NotesAPI.deleteNote(id)
      .then(() => NotesAPI.fetchNotes())
      .then(data => setNotes(data.notes))
  }

  const handleEditNote = (id, text) => {
    return NotesAPI.editNote(id, text)
      .then(() => NotesAPI.fetchNotes())
      .then(data => setNotes(data.notes))
  }

  return (
    <Container
      className="d-flex flex-column align-items-center pb-5"
      style={{height: window.innerHeight - 54}}
    >
      <h3 className="text-center mb-4">Notes</h3>

      <Form className="d-flex flex-column mb-3">
        <Button className="mb-2" onClick={() => setShowCreateNote(!showCreateNote)}>Add note</Button>
        {showCreateNote && (<>
          <Form.Control
            className="mb-2"
            placeholder="Enter note text"
            value={newNoteText}
            onChange={e => setNewNoteText(e.target.value)}
          />
          <Button className="mb-2 btn-success" onClick={() => handleAddNote()}>Create note</Button>
        </>)}
      </Form>

      {isLoading
        ?
        <p>Loading...</p>
        :
        <>
          {notes.length
              ?
              <ListGroup className="col-9">
                {notes.map(note => (
                  <NoteItem
                    key={note._id}
                    note={note}
                    checkNote={handleCheckNote}
                    deleteNote={handleDeleteNote}
                    editNote={handleEditNote}
                  />
                ))}
              </ListGroup>
              :
              <p>Notes not found</p>
          }
        </>
      }



    </Container>
  );
}

export default Notes;
