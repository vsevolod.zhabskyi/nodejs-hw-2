import React, {useEffect, useState} from 'react';
import {Button, Card, Container, Form, ListGroup, ListGroupItem} from "react-bootstrap";
import {deleteUser, fetchUser, updatePassword} from '../API/userAPI';
import {useNavigate} from 'react-router-dom';
import {LOGIN_ROUTE} from '../utils/routes';

function Profile() {

  const [userInfo, setUserInfo] = useState(null);
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [newPasswordRepeat, setNewPasswordRepeat] = useState('');
  const [showChangePassword, setShowChangePassword] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate()

  useEffect(() => {
    fetchUser().then(data => {
      setUserInfo(data);
      setIsLoading(false)
    });
  }, [])

  const handleChangePassword = async () => {
    if (newPassword !== newPasswordRepeat) {
      alert('Passwords do not match')
      return
    }
    await updatePassword(oldPassword, newPassword)
      .then(() => {
        alert('Success')
        setShowChangePassword(false)
      })
      .catch((e) => alert(e.message))
  }

  const handleDeleteUser = async () => {
    await deleteUser()
      .then(() => {
        alert('Success')
        navigate(LOGIN_ROUTE)
      })
      .catch((e) => alert(e.message))
  }

  return (

    <Container
      className="d-flex justify-content-center align-items-center"
      style={{height: window.innerHeight - 54}}
    >
      <Card className="col-6 p-4">
        {!isLoading && (<>
          <ListGroup className="mb-3">
            <ListGroupItem><p>Username: {userInfo?.username}</p></ListGroupItem>
            <ListGroupItem><p>Id: {userInfo?._id}</p></ListGroupItem>
            <ListGroupItem><p>Registration date: {userInfo?.createdDate}</p></ListGroupItem>
          </ListGroup>

          <a className="mb-3" onClick={() => setShowChangePassword(!showChangePassword)}>Change Password</a>

          {showChangePassword && (
            <Form className="d-flex flex-column mb-3">
              <Form.Label>Old password</Form.Label>
              <Form.Control
                className="mb-2"
                placeholder="Old password..."
                value={oldPassword}
                onChange={e => setOldPassword(e.target.value)}
                type="password"
              />

              <Form.Label>New password</Form.Label>
              <Form.Control
                className="mb-2"
                placeholder="New password..."
                value={newPassword}
                onChange={e => setNewPassword(e.target.value)}
                type="password"
              />

              <Form.Label>Repeat new password</Form.Label>
              <Form.Control
                className="mb-2"
                placeholder="New password..."
                value={newPasswordRepeat}
                onChange={e => setNewPasswordRepeat(e.target.value)}
                type="password"
              />
              <Button onClick={() => handleChangePassword()}>Change Password</Button>
            </Form>
          )}

          <Button className="btn-danger" onClick={() => handleDeleteUser()}>Delete User</Button>
        </>)}
      </Card>
    </Container>
  );
}

export default Profile;
