import React, {useContext, useState} from 'react';
import {Button, Card, Container, Form} from "react-bootstrap";
import {LOGIN_ROUTE, REGISTRATION_ROUTE, NOTES_ROUTE} from "../utils/routes";
import {Link, useNavigate, useLocation} from "react-router-dom";
import * as authAPI from "../API/authAPI";
import {observer} from "mobx-react-lite";
import {Context} from "../index";

const LoginRegister = observer( () => {
  const {user} = useContext(Context)
  const location = useLocation()
  const navigate = useNavigate()
  const isLogin = location.pathname === LOGIN_ROUTE
  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')

  const click = async () => {
    try {
      let data;
      if (isLogin) {
        data = await authAPI.login(login, password)
      } else {
        data = await authAPI.registration(login, password)
      }
      user.setUser(data)
      user.setIsAuth(true)
      navigate(NOTES_ROUTE)

    } catch(e) {
      alert(e.response.data.message)
    }
  }

  return (
    <Container
      className="d-flex justify-content-center align-items-center"
      style={{height: window.innerHeight - 54}}
    >
      <Card style={{width: 600}} className="p-4">
        <h2 className="m-auto mb-3">{isLogin ? 'Log in' : 'Sign up'}</h2>
        <Form className="d-flex flex-column">
          <Form.Control
            className="mb-3"
            placeholder="Login..."
            value={login}
            onChange={e => setLogin(e.target.value)}
          />
          <Form.Control
            className="mb-3"
            placeholder="Password..."
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
          <div className="d-flex flex-row justify-content-between">
            {isLogin ?
              <div>
                Don't have an account yet? <Link to={REGISTRATION_ROUTE}>Sign up</Link>
              </div>
              :
              <div>
                Have an account? <Link to={LOGIN_ROUTE}>Log in</Link>
              </div>
            }

            <Button  variant={"outline-dark"}
                     className="mt-2 px-4"
                     onClick={click}
            >
              {isLogin ?
                "Log in"
                :
                "Sign up"
              }
            </Button>
          </div>

        </Form>
      </Card>

    </Container>
  );
})

export default LoginRegister;
